<?php

namespace Drupal\pdfjsviewer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Returns responses for aggregator module routes.
 */
class PDFJsController extends ControllerBase {

  /**
   * Displays the pdf.
   *
   * @return array
   *   A render array as expected by drupal_render().
   */
  public function reportsoverview() {
		return [
      '#theme' => 'pdf_render',
    ];
	}
}